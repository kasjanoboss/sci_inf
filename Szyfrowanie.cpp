﻿#include <iostream>
#include <string>
using namespace std;


string SzyfrPodstawieniowy(string tekst) {
	int dlugosc = tekst.length();
	string zmieniony;
	int klucz;
	cout << "Podaj klucz szyfrowania od 1 do 26";
	cin >> klucz;
	unsigned char znak;
	if (klucz >= 1 && klucz <= 26) {
		for (int i = 0; i < dlugosc; i++) {
			if (tekst[i] == ' ') {
				i + 1;
			}
			else {
				if (tekst[i] + klucz >= 123) {
					znak = ((tekst[i] + klucz) % 123) + 97;
					zmieniony += znak;
				}
				else if (tekst[i] > 96 && tekst[i] < 123) {
					znak = tekst[i] + klucz;
					zmieniony += znak;
				}
				else {
					zmieniony += tekst[i];
				}
			}
		}
	}
	else {
		return "Zly klucz, nie miesci sie w zakresie";

	}
	return zmieniony;
}

string SzyfrPrzestawieniowy(string tekst) {

	int dlugosc = tekst.length();
	string zmieniony;


	for (int i = 0; i < dlugosc; i += 2) {
		swap(tekst[i], tekst[i + 1]);
		zmieniony += tekst[i];
		zmieniony += tekst[i + 1];
	}
	if (dlugosc % 2 == 1) {
		zmieniony += tekst[dlugosc - 1];
	}
	return zmieniony;
}

string DSzyfrPodstawieniowy(string tekst) {

	int dlugosc;
	dlugosc = tekst.length();
	string zmieniony;
	int klucz;
	cout << "Podaj klucz szyfrowania (1-26): ";
	cin >> klucz;
	unsigned char znak;

	if (klucz >= 1 && klucz <= 26) {
		for (int i = 0; i < dlugosc; i++) {
			if (tekst[i] == ' ') {
				i + 1;
			}
			else {
				if ((tekst[i] - klucz) < 97) {
					znak = tekst[i] - klucz;
					char odszyfrowanie = (tekst[i] % 97) + 123;
					znak = odszyfrowanie - klucz;
					zmieniony += znak;
				}
				else if (tekst[i] >= 96) {
					znak = tekst[i] - klucz;
					zmieniony += znak;
				}
				else {
					zmieniony += tekst[i];
				}

			}
		}
	}
	return zmieniony;

}

int main() {
	string znaki;
	int jakiAlgorytm;
	cout << "Podaj ciag liter: ";
	getline(cin, znaki);
	cout << "Wybierz algorytm: " << endl;
	cout << "1. Szyfr podstawieniowy(szyfr cezara)" << endl;
	cout << "2. Szyfr przestawieniowy" << endl;
	cout << "3. Szyfr podstawieniowy i szyfr przestawieniowy" << endl;
	cout << "4. Odszyfruj tekst" << endl;
	cin >> jakiAlgorytm;
	if (jakiAlgorytm == 1) {
		cout << SzyfrPodstawieniowy(znaki);
	}
	else if (jakiAlgorytm == 2) {
		cout << SzyfrPrzestawieniowy(znaki);
	}
	else if (jakiAlgorytm == 3)
	{
		cout << SzyfrPodstawieniowy(SzyfrPrzestawieniowy(znaki));
	}
	else if (jakiAlgorytm == 4) {
		int jaki = 0;
		cout << "jaki algorytm chcesz odszyfrować?" << endl;
		cout << "1. Szyfr podstawieniowy (szyfr cezara)" << endl;
		cout << "2. Szyfr przestawieniowy" << endl;
		cin >> jaki;
		if (jaki == 1) {
			cout << DSzyfrPodstawieniowy(znaki);
		}
		else if (jaki == 2) {
			cout << SzyfrPrzestawieniowy(znaki);
		}
		else {
			cout << "nie ma takiego szyfru";
		}
	}
	else {
		cout << "nie ma takiego algorytmu";
	}
	return 0;
}