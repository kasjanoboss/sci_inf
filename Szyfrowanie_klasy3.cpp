﻿#include <iostream>
#include <string>
using namespace std;

class cezar {
public:
	string tekst;
	string zmieniony;
	int klucz;
	cezar(string tekst) {
		this->tekst = tekst;
	}


	string SzyfrPodstawieniowy() {

		cout << "Podaj klucz szyfrowania od 1 do 26" << endl;
		cin >> klucz;
		unsigned char znak;
		if (klucz >= 1 && klucz <= 26) {
			for (int i = 0; i < tekst.length(); i++) {
				if ((tekst[i] >= 48 && tekst[i] <= 57) || tekst[i] == ' ') {
					zmieniony += tekst[i];
					continue;
				}
				else {
					if (tekst[i] + klucz >= 123) {
						znak = ((tekst[i] + klucz) % 123) + 97;
						zmieniony += znak;
					}
					else if (tekst[i] > 96 && tekst[i] < 123) {
						znak = tekst[i] + klucz;
						zmieniony += znak;
					}
					else {
						zmieniony += tekst[i];
					}
				}
			}
		}
		else {
			return "Zly klucz, nie miesci sie w zakresie";

		}
		return zmieniony;
	}
};


class przestawieniowy {
public:
	string tekst;
	string zmieniony;
	int dlugosc = tekst.length();

	przestawieniowy(string tekst) {
		this->tekst = tekst;
	}
	string SzyfrPrzestawieniowy() {
		for (int i = 0; i < dlugosc; i += 2) {
			swap(tekst[i], tekst[i + 1]);
			zmieniony += tekst[i];
			zmieniony += tekst[i + 1];
		}
		if (dlugosc % 2 == 1) {
			zmieniony += tekst[dlugosc - 1];
		}
		return zmieniony;
	}
};
class DCezar {
public:
	int dlugosc;
	int u;
	string tekst;

	DCezar(string tekst, int u) {
		this->tekst = tekst;
		this->u = u;
	}
	string zmieniony;
	unsigned char znak;

	string DSzyfrPodstawieniowy() {
		for (int i = 0; i < tekst.length(); i++) {
			if ((tekst[i] >= 48 && tekst[i] <= 57) || tekst[i] == ' ') {
				zmieniony += tekst[i];
				continue;
			}
			if ((tekst[i] - u) < 97) {
				znak = tekst[i] - u;
				char odszyfrowanie = (tekst[i] % 97) + 123;
				znak = odszyfrowanie - u;
				zmieniony += znak;
			}
			else if (tekst[i] >= 96) {
				znak = tekst[i] - u;
				zmieniony += znak;
			}
			else {
				zmieniony += tekst[i];
			}
		}
		return zmieniony;
	}
};


int main() {

	string znaki;
	int jakiAlgorytm;
	cout << "Podaj ciag liter: ";
	getline(cin, znaki);
	cout << "Wybierz algorytm: " << endl;
	cout << "1. Szyfr podstawieniowy(szyfr cezara)" << endl;
	cout << "2. Szyfr przestawieniowy" << endl;
	cout << "3. Szyfr podstawieniowy i szyfr przestawieniowy" << endl;
	cout << "4. Odszyfruj tekst" << endl;
	cin >> jakiAlgorytm;
	if (jakiAlgorytm == 1) {
		cezar obiekt1(znaki);
		cout << obiekt1.SzyfrPodstawieniowy();
	}
	else if (jakiAlgorytm == 2) {
		przestawieniowy obiekt2(znaki);
		cout << obiekt2.SzyfrPrzestawieniowy();
	}
	else if (jakiAlgorytm == 3)
	{
		cezar obiekt1(znaki);
		przestawieniowy obiekt2(znaki);
		cout << obiekt1.SzyfrPodstawieniowy();

	}
	else if (jakiAlgorytm == 4) {

		int jaki = 0;
		cout << "jaki algorytm chcesz odszyfrować?" << endl;
		cout << "1. Szyfr podstawieniowy (szyfr cezara)" << endl;
		cout << "2. Szyfr przestawieniowy" << endl;
		cin >> jaki;
		if (jaki == 1) {
			for (int u = 0; u <= 25; u++) {
				DCezar obiekt3(znaki, u);
				cout << obiekt3.DSzyfrPodstawieniowy() << endl;
			}
		}
		else if (jaki == 2) {
			przestawieniowy obiekt2(znaki);
			cout << obiekt2.SzyfrPrzestawieniowy();
		}
		else {
			cout << "nie ma takiego szyfru";
		}
	}
	else {
		cout << "nie ma takiego algorytmu";
	}
	return 0;
}